﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Neogov.Common.Web.NeoGrid;
using TestApp.Models;

namespace TestApp.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            var viewModel = new HomeIndexViewModel();
            var columns = new List<Tuple<string, Type>>
                {
                    Tuple.Create("IntegerField", typeof (int)),
                    Tuple.Create("StringField", typeof (string)),
                    Tuple.Create("DateField", typeof (DateTime)),
                    Tuple.Create("SelectField", typeof (string))
                };
            viewModel.NeoGridViewModel = new NeoGridViewModel("neogrid-table", "Home/GetDataForGrid", columns, new List<bool> { true, true, true, false })
                {ColumnNames = new List<string> {"NUMERO", "CADENA", "FECHA", "SELLECTIONAR"}};
            viewModel.NeoGridViewModel.FilterOn("SelectField").Select(viewModel.Majors);
            viewModel.NeoGridViewModel.FilterOn("IntegerField").Number();
            viewModel.NeoGridViewModel.FilterOn("DateField").DateRange();
            
            viewModel.NeoGridViewModel.ColumnFilter = true;
            
            return View(viewModel);
        }

        //
        // POST: /
        public NeoGridResult<DataItem> GetDataForGrid([ModelBinder(typeof(NeoGridModelBinder))]NeoGridRequestModel requestModel)
        {
            var gridPayload = DataItemProvider.GetData(30).ToArray();
            
            const int totalRecs = 30;
            var totalDisplayed = requestModel.DisplayLength;
            var data=gridPayload.Skip((requestModel.DisplayStart - 1)*requestModel.DisplayLength).Take(requestModel.DisplayLength).ToArray();
            return new NeoGridResult<DataItem>(requestModel,totalRecs,totalRecs,data);
        }
    }
}
