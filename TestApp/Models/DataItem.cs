﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

namespace TestApp.Models
{
    [DataContract]
    public class DataItem
    {
        [DataMember]
        public int IntegerField { get; set; }

        [DataMember]
        public string StringField { get; set; }

        [DataMember]
        public string DateField { get; set; }

        [DataMember]
        public string SelectField { get; set; }

        public DataItem(int integer,string str,DateTime dt,string sel)
        {
            IntegerField = integer;
            StringField = str;
            DateField = dt.ToString(CultureInfo.InvariantCulture);
            SelectField = sel;
        }
    }

    public class DataItemProvider
    {
        public static IEnumerable<DataItem> GetData(int count)
        {
            var items = new List<DataItem>();

            for (int i = 0; i < count; i++)
            {
                items.Add(new DataItem(i,"String"+i,DateTime.Now.AddDays(i),"Select"));    
            }

            return items;
        }
    }
}