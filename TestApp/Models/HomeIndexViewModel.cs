﻿using Neogov.Common.Web.NeoGrid;

namespace TestApp.Models
{
    public class HomeIndexViewModel
    {
        public HomeIndexViewModel()
        {
            Majors = new[]
                {
                    "Computer Science",
                    "Computer Engineering",
                    "Electrical Engineering",
                    "Aerospace Engineering"
                };
        }

        public string[] Majors { get; set; }

        public NeoGridViewModel NeoGridViewModel { get; set; }  
    }
}